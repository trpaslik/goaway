document.addEventListener('mousedown', function(ev) {
  'use strict';
  if (ev.shiftKey && ev.altKey) { 
    ev.target.remove();
    ev.preventDefault();
    ev.stopPropagation();
    ev.stopImmediatePropagation();
  }
}, true);

document.addEventListener('keydown', function(ev) {
  'use strict';
  var videos;
  console.log(ev);
  if (ev.shiftKey && ev.altKey && ev.key.substr(0, 5) === 'Arrow') {
    videos = document.querySelectorAll('video');
    [].forEach.call(videos, function(v) {
      //console.log(v);
      if (!v.paused) {
        console.log('is playing');
        switch (ev.key) {
          case 'ArrowRight': 
            v.currentTime = v.seekable.end(0);
            break;
          case 'ArrowLeft': 
            v.playbackRate = v.defaultPlaybackRate;
            break;
          case 'ArrowUp': 
            v.playbackRate += 0.1;
            break;
          case 'ArrowDown': 
            v.playbackRate -= 0.1;
            break;
        }
        ev.preventDefault();
        ev.stopPropagation();
      }
    }); 
  }
}, true);
